using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// using FMODUnity;
// using FMOD;
// using FMOD.Studio;
// using FMODUnityResonance;
using UnityEngine.InputSystem;

public class FreeFlyCamera : MonoBehaviour
{
	private const float ANGLE_EPSILON = 0.001f;
	
	private PlayerInput playerInput;
	
	public KeyCode keyLeft       = KeyCode.A;
	public KeyCode keyRight      = KeyCode.D;
	public KeyCode keyForward    = KeyCode.W;
	public KeyCode keyBackward   = KeyCode.S;
	public KeyCode keyUp         = KeyCode.E;
	public KeyCode keyDown       = KeyCode.Q;
	public float moveSpeed       = 0.3f;
	public float lookSensitivity = 0.05f;
	public Vector3 upVec         = Vector3.up;
	
	private Vector2 lookRotation;
	
	// Start is called before the first frame update
	void Start()
	{
		lookRotation.x = this.transform.rotation.eulerAngles.x;
		lookRotation.y = this.transform.rotation.eulerAngles.y;
		
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
		
		playerInput = GetComponent<PlayerInput>();
	}
	
	// Update is called once per frame
	void FixedUpdate()
	{
		float tDelta = (Time.deltaTime / (1.0f/60.0f));
		
		if (true) //TODO: Make this togglable
		{
			Vector2 viewRotationInput = playerInput.actions["ViewRotation"].ReadValue<Vector2>();
			if (viewRotationInput.x != 0)
			{
				lookRotation.x -= viewRotationInput.x * lookSensitivity;// * Time.timeScale;
			}
			if (viewRotationInput.y != 0)
			{
				lookRotation.y += viewRotationInput.y * lookSensitivity;// * Time.timeScale;
			}
			
			if (lookRotation.x >= (float)Math.PI*2) { lookRotation.x -= (float)Math.PI*2; }
			if (lookRotation.x < 0) { lookRotation.x += (float)Math.PI*2; }
			if (lookRotation.y > (float)Math.PI/2 - ANGLE_EPSILON) { lookRotation.y = (float)Math.PI/2 - ANGLE_EPSILON; }
			if (lookRotation.y < (float)-Math.PI/2 + ANGLE_EPSILON) { lookRotation.y = (float)-Math.PI/2 + ANGLE_EPSILON; }
		}
		
		Vector3 lookVec = new Vector3((float)Math.Cos(lookRotation.y) * (float)Math.Cos(lookRotation.x), (float)Math.Sin(lookRotation.y), (float)Math.Cos(lookRotation.y) * (float)Math.Sin(lookRotation.x));
		this.transform.LookAt(this.transform.position + lookVec, upVec);
		
		Vector3 forwardVec     = -this.transform.forward;
		Vector3 rightVec       = Vector3.Cross(forwardVec, upVec);
		Vector3 flatForwardVec = Vector3.Cross(rightVec, upVec);
		
		if (playerInput.actions["Right"].phase == InputActionPhase.Started)
		{
			this.transform.position += rightVec * moveSpeed;// * Time.timeScale;
		}
		if (playerInput.actions["Left"].phase == InputActionPhase.Started)
		{
			this.transform.position -= rightVec * moveSpeed;// * Time.timeScale;
		}
		if (playerInput.actions["Forward"].phase == InputActionPhase.Started)
		{
			this.transform.position += flatForwardVec * moveSpeed;// * Time.timeScale;
		}
		if (playerInput.actions["Backward"].phase == InputActionPhase.Started)
		{
			this.transform.position -= flatForwardVec * moveSpeed;// * Time.timeScale;
		}
		if (playerInput.actions["Up"].phase == InputActionPhase.Started)
		{
			this.transform.position += upVec * moveSpeed;// * Time.timeScale;
		}
		if (playerInput.actions["Down"].phase == InputActionPhase.Started)
		{
			this.transform.position -= upVec * moveSpeed;// * Time.timeScale;
		}
	}
}
