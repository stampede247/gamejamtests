using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteHandler : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;
    private Sprite[] entitiesSprites;
    private string currentSpriteName = null;
    private float animTime = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        entitiesSprites = Resources.LoadAll<Sprite>("Sprites/entitiesAtlas");
        //Debug.Log($"Found {entitiesSprites.Length} sprites");
    }

    float TimeScaledAnim(float animPeriodMs)
    {
        return (Time.deltaTime * 1000) / animPeriodMs;
    }

    // Update is called once per frame
    void Update()
    {
    	//Debug.Log($"deltaTime: {Time.deltaTime}");
        this.animTime += TimeScaledAnim(1000);
        this.animTime = Mathf.Repeat(this.animTime, 1.0f);
        int frameNum = Mathf.FloorToInt(this.animTime * 6);
        //string newSpriteName = "Sprites/entitiesAtlas:disruptor_" + (1 + frameNum);
        //if (newSpriteName != currentSpriteName)Assets/Resources/Sprites/entitiesAtlas.png
        //{
        //    spriteRenderer.sprite = Resources.Load<Sprite>(newSpriteName);
        //    Debug.Log($"Loading Sprite {newSpriteName} ({spriteRenderer.sprite})");
        //    currentSpriteName = newSpriteName;
        //}
        spriteRenderer.sprite = entitiesSprites[entitiesSprites.Length - 1 - frameNum];
    }
}
