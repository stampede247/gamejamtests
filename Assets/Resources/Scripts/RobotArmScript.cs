using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Assertions;

public class RobotArmScript : MonoBehaviour
{
	public Transform armature;
	public SkinnedMeshRenderer mainMesh;
	public Animator animatorComponent;
	
	private PlayerInput playerInput;
	private Transform armBone;
	private Transform handBone;
	private Transform fingerBone1;
	private Transform fingerBone2;
	
	// Start is called before the first frame update
	void Start()
	{
		armBone = armature.Find("root_bone").Find("stand_bone").Find("forearm_bone");
		handBone = armBone.Find("hand_bone");
		fingerBone1 = handBone.Find("finger1_bone");
		fingerBone2 = handBone.Find("finger2_bone");
		Assert.IsNotNull(armBone, "Robot Arm Bone Missing");
		Assert.IsNotNull(handBone, "Robot Hand Bone Missing");
		Assert.IsNotNull(fingerBone1, "Robot Finger1 Bone Missing");
		Assert.IsNotNull(fingerBone2, "Robot Finger2 Bone Missing");

        playerInput = GetComponent<PlayerInput>();
        // animationComponent.AddClip(waveAnim, "WaveAnim");
    }
	
	// Update is called once per frame
	void Update()
	{
		float tDelta = (Time.deltaTime / (1.0f/60.0f));

        // if (armBone != null)
        // {
        // 	armBone.Rotate(new Vector3(0.2f, 0, 0) * tDelta, Space.Self);
        // 	mainMesh.materials[2].color = new Color(1, armBone.rotation.eulerAngles.x / 360.0f, 0, 1);
        // 	// Debug.Log($"Arm Rotaiton: {armBone.rotation.eulerAngles}");
        // }
        // if (handBone != null)
        // {
        // 	handBone.Rotate(new Vector3(0, 0.3f, 0) * tDelta, Space.Self);
        // }

        if (playerInput.actions["Test"].triggered && animatorComponent != null)
        {
            Debug.Log("Waving!");
            animatorComponent.SetBool("isWaving", true);
            animatorComponent.Update(0.0f);
            animatorComponent.SetBool("isWaving", false);
            FMODUnity.RuntimeManager.PlayOneShot("event:/MainEvents/RobotHello", this.transform.position);
        }
    }
}
